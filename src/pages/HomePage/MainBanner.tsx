import React from "react";

const MainBanner = () => {
  return (
    <>
      <div className="main-section">
        <div className="banner-bf">
          <img src="./images/banner-bf.svg" alt="" />
        </div>
        <div className="container">
          <div className="main-body">
            <div className="columns align-center">
              <div className="colum w-7">
                <div className="content-banner">
                  <h3 className="title">
                    <img src="./images/line-title.svg" alt="" />
                    Réduisez votre empreinte carbone avec l'aide de
                    <span className="main-color">vos usagers !</span>
                  </h3>
                  <p className="desc">
                    L’application qui vous permet d'inciter vos usagers ou vos
                    clients à effectuer les écogestes que vous souhaitez. Vous
                    les aider à changer, ils vous aident à réduire l'empreinte
                    carbone de votre activité.
                  </p>
                  <div className="button-banner">
                    <button type="button" className="btn btn-angel">
                      Devenir un Carbon Angel
                    </button>
                    <button type="button" className="btn btn-devenir">
                      Devenir un Carbon Actor{" "}
                      <img src="./images/icons.svg" alt="" />
                    </button>
                  </div>
                </div>
              </div>
              <div className="colum w-5">
                <div className="box-img">
                  <img src="./images/banner.svg" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default MainBanner;
