import React from "react";

const RoundDesc = () => {
    return (
        <>
            <div className="main-section bg-fafafa">
                <div className="arrow-l">
                    <img src="./images/arrow-l.svg" alt="" />
                </div>
                <div className="arrow-r">
                    <img src="./images/arrow-r.svg" alt="" />
                </div>
                <div className="container">
                    <div className="desc-round">
                        <div className="content-desc">
                            <div className="title">
                                Utilisez un nouveau levier pour réduire vos émissions : l'engagement de vos usagers !  
                            </div>
                            <div className="desc">
                                Notre objectif est simple :   Réussir, avec vous et la mobilisation de vos usagers, à réduire les <br /> émissions de 1 Mln tonnes de CO2 à l’horizon de 5 ans
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default RoundDesc