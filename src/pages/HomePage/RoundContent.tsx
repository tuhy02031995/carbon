import React from "react";

const RoundContent = () => {
    return (
        <>
            <div className="main-section">
                <div className="container">
                    <div className="content-round">
                        <div className="main-title">
                            <div className="icon">
                            <img src="./images/line-title.svg" alt="" />
                            </div>
                            Il y a urgence à agir <span className="main-color">pour le climat</span>
                        </div>
                        <div className="table-round">
                            <div className="columns">
                                <div className="colum w-4">
                                    <div className="wrap-content">
                                        <div className="box-icon">
                                            <img src="./images/round-1.svg" alt="" />
                                        </div>
                                        <div className="title">
                                            50 % des français et <br /> 75 % des jeunes
                                        </div>
                                        <div className="desc">
                                            Se disent prêts à réaliser des changements dans leur mode de vie pour préserver l’environnement. (enquête IPSOS, CREDOC). Malgré cela, peu encore passent à l’acte.
                                        </div>
                                    </div>
                                </div>
                                <div className="colum w-4">
                                    <div className="wrap-content">
                                        <div className="box-icon">
                                            <img src="./images/round-2.svg" alt="" />
                                        </div>
                                        <div className="title">
                                            60 % des trajets <br /> en voitures
                                        </div>
                                        <div className="desc">
                                            C’est la part des trajets domicile-travail de moins de 5 km qui sont effectués en voiture. Sur de telles distances, un vélo est aussi rapide et n’émet aucun gaz à effet de serre.
                                        </div>
                                    </div>
                                </div>
                                <div className="colum w-4">
                                    <div className="wrap-content">
                                        <div className="box-icon">
                                            <img src="./images/round-3.svg" alt="" />
                                        </div>
                                        <div className="title">
                                            3 tonnes de CO2 <br /> sur 4
                                        </div>
                                        <div className="desc">
                                            C’est la proportion des émissions carbone liées à la venue des clients ou usagers de nombreuses activités – services, hôtellerie, culture, loisirs. 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default RoundContent