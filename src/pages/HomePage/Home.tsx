import React from 'react';
import MainBanner  from './MainBanner';
import RoundContent from './RoundContent';
import RoundDesc from './RoundDesc';
import MainService from './MainService';
import MainAbout from './MainAbout';
import MainComment from './MainComment';
import MainChanger from './MainChanger';
import './style.css'
// import 'antd/dist/antd.css';

const Home = () => {
    return (
        <>
            <MainBanner />
            <RoundContent />
            <RoundDesc />
            <MainService />
            <MainAbout />
            <MainComment />
            <MainChanger />
        </>
    )
}
export default Home