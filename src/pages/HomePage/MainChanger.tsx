import React from "react";

const MainChanger = () => {
  return (
    <>
      <div className="main-section">
        <div className="container">
          <div className="content-changer">
            <div className="main-title">
              <div className="icon res">
                <img src="./images/line-big.svg" alt="" />
              </div>
              Envie de changer et de faire changer autour de vous ? <br />
              <span className="main-color">Rejoignez nous !</span>
            </div>
            <div className="table-changer">
              <div className="columns">
                <div className="colum w-6">
                  <div className="content-guide">
                    <div className="title">
                      Acteurs du monde culturel ou associatif, élus, chefs
                      d’entreprises ,
                    </div>
                    <div className="desc">
                      Vous avez un pouvoir simple et immense auprès de votre
                      communauté de clients, de fans, ou d’usagers : <br />{" "}
                      <br />
                      Dites leur que vous souhaitez réduire votre impact carbone
                      avec eux et aidez-les à changer grâce à notre application
                      et à vos incitations.
                    </div>
                    <div className="button-banner">
                      <button type="button" className="btn btn-angel">
                        Devenir un Carbon Angel
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-6">
                  <div className="line-bottom">
                    <img src="./images/line-bottom.svg" alt="" />
                  </div>
                  <div className="content-guide">
                    <div className="title">
                      Vous êtes un particulier et souhaitez participer au
                      changement ?
                    </div>
                    <div className="desc">
                      Faire connaître son engagement, ou se faire aider car
                      changer ses pratiques n’est pas si simple : <br /> <br />
                      Rejoignez 1er plateforme de rétribution des gestes
                      écologiques et montrez que vous aussi, vous participer à
                      préserver la planète !
                    </div>
                    <div className="button-banner">
                      <button type="button" className="btn btn-devenir">
                        Devenir un Carbon Actor{" "}
                        <img src="./images/icons.svg" alt="" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-massage">
               <div className="title">
               Pour suivre toute nos actus
               </div>
                <div className="button-banner">
                      <button type="button" className="btn btn-angel">
                        Nous suivre <img src="./images/LinkedIn.svg" alt="" />
                      </button>
                    </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default MainChanger;
