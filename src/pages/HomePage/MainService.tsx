import React from "react";

const MainService = () => {
  return (
    <>
      <div className="main-section">
        <div className="container">
          <div className="main-service">
            <div className="table-servide">
              <div className="columns align-center">
                <div className="colum w-6">
                  <div className="content-service">
                    <div className="title">
                      Une solution intégrée de services à votre disposition
                    </div>
                    <ul className="list-sv">
                      <li>
                        <div className="guide-l">
                          <div className="box-img">
                            <img src="./images/Vector.svg" alt="" />
                          </div>
                        </div>
                        <div className="guide-r">
                          <div className="txt">
                            Une appli pour inciter vos usagers
                          </div>
                          <div className="desc">
                            Utiliser notre application pour inciter vos clients
                            à venir en vélo, en covoiturage, ou à consommer plus
                            durable, ou tout geste vertueux certifiable.
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="guide-l">
                          <div className="box-img">
                            <img src="./images/Vector.svg" alt="" />
                          </div>
                        </div>
                        <div className="guide-r">
                          <div className="txt">
                            Une aide pour présenter les alternatives bas-carbone
                          </div>
                          <div className="desc">
                            Aussi, aidez vos usagers à changer grâce à la mise
                            en avant au bon moment des alternatives bas-carbone
                            existantes.
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="guide-l">
                          <div className="box-img">
                            <img src="./images/Vector.svg" alt="" />
                          </div>
                        </div>
                        <div className="guide-r">
                          <div className="txt">
                            Un outil qui quantifie vos réductions d'émission
                          </div>
                          <div className="desc">
                            Grâce à l'application, vous connaissez mieux
                            l'empreinte carbone de vos usagers. Et nous vous
                            aidons à valoriser vos réductions en allant chercher
                            des financements climat.
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="guide-l">
                          <div className="box-img">
                            <img src="./images/Vector.svg" alt="" />
                          </div>
                        </div>
                        <div className="guide-r">
                          <div className="txt">
                            Une plateforme pour mieux communiquer
                          </div>
                          <div className="desc">
                            Carbon-angel vise à créer et fédérer une communauté
                            d'acteurs engagés. Sa plateforme en ligne permet de
                            communiquer vos engagements et vos résultats de
                            manière renforcée et ciblée, sans greenwashing.
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div className="button-banner">
                      <button type="button" className="btn btn-angel">
                        Devenir un Carbon Angel
                      </button>
                      <button type="button" className="btn btn-devenir">
                        Devenir un Carbon Actor{" "}
                        <img src="./images/icons.svg" alt="" />
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-6">
                  <div className="box-banner">
                    <img src="./images/banner-sv.svg" alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default MainService;
