import React from "react";

const MainComment = () => {
    return (
        <>
            <div className="main-section">
                <div className="container">
                    <div className="content-comment">
                        <div className="main-title">
                            Comment ça marche ?
                        </div>
                        <ul className="list-comment">
                            <li>
                                <div className="title">
                                    Rejoindre la communauté des <br /> Carbon Angel
                                </div>
                                <div className="desc">
                                    J’accède à un outil en ligne de communication <br /> vers mes usagers et de vérification des écogestes <br />que je cherche à inciter.
                                </div>
                            </li>
                            <li>
                                <div className="title">
                                Mise en place de projets <br /> d’incitation aux écogestes
                                </div>
                                <div className="desc">
                                Je choisis les écogestes qui réduisent le plus l’impact de mon <br /> activité et je définis une/des récompense(s) directes pour tous <br /> ceux qui les adopteront 
                                </div>
                            </li>
                            <li>
                                <div className="title">
                                Suivi & vérification sur <br /> l’application mobile
                                </div>
                                <div className="desc">
                                Tous mes usagers qui auront adopté les gestes choisis, deviennent mes carbon <br /> actors. Je dispose ainsi d’une communauté d’usagers engagés que je cherche à <br /> faire grossir et à aider dans ses choix écologiques. 

                                </div>
                            </li>
                            <li>
                                <div className="title">
                                Valoriser la réduction de ses<br /> impacts
                                </div>
                                <div className="desc">
                                L’application m’aide à quantifier les réduction d’émissions obtenues par mes <br /> incitations, à mentionner dans mon rapport d’activités. Carbon-Angel m’accompagne <br /> pour une valorisation économique de ces réductions de GES, pour le partage<br /> d’expérience entre carbon angels.
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}
export default MainComment