import React from "react";

const MainAbout = () => {
  return (
    <>
      <div className="main-section">
        <div className="container">
          <div className="content-about">
            <div className="elip-ab">
                <img src="./images/elip-ab.svg" alt="" />
            </div>
            <div className="line-ab">
                <img src="./images/line-ab.svg" alt="" />
            </div>
            <div className="table-about">
              <div className="columns">
                <div className="colum w-6">
                  <div className="content-about-l">
                    <div className="main-title res">
                      <div className="icon">
                        <img src="./images/line-title.svg" alt="" />
                      </div>
                      <img src="./images/but.svg" alt="" className="but-img" />
                      Pourquoi devenir <br />{" "}
                      <span className="main-color">Carbon-angel ?</span>
                    </div>
                    <div className="desc">
                      Les raisons ne manquent pas de vous lancer à nos côtés
                      dans l’aventure Carbon-angel !
                    </div>
                    <div className="button-banner">
                      <button type="button" className="btn btn-angel">
                        Devenir un Carbon Angel
                      </button>
                      <button type="button" className="btn btn-devenir">
                        Devenir un Carbon Actor{" "}
                        <img src="./images/icons.svg" alt="" />
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-6">
                    <ul className="line-about">
                        <div className="arrow-ab">
                        <img src="./images/arrow-ab.svg" alt="" />
                        </div>
                        <li>
                            <div className="icon-ab">
                                <img src="./images/icon-ab.svg" alt="" />
                            </div>
                            <div className="text-ab">
                                <div className="title">
                                    Réduire votre impact écologique
                                </div>
                                <div className="desc">
                                    Vous incitez vos clients à adopter les gestes écologiques et <br /> vous réduisez ainsi de manière quantifiée et certifiée <br />  l'empreinte de votre activité.
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="icon-ab">
                                <img src="./images/icon-ab-v2.svg" alt="" />
                            </div>
                            <div className="text-ab">
                                <div className="title">
                                Pour inciter le plus grand nombre à changer
                                </div>
                                <div className="desc">
                                Changer en interne, c'est bien. Changer avec ses clients ou <br /> usagers, c'est mieux et ca va plus loin !
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="icon-ab">
                                <img src="./images/icon-ab-v3.svg" alt="" />
                            </div>
                            <div className="text-ab">
                                <div className="title">
                                Attirer de nouveaux utilisateurs via votre image
                                </div>
                                <div className="desc">
                                En récompensant les écogestes, vous attirez à vous les <br /> usagers sensibles à l'écologie, mais aussi ceux à la recherche <br /> de bonnes affaires !
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="icon-ab">
                                <img src="./images/icon-ab-v4.svg" alt="" />
                            </div>
                            <div className="text-ab">
                                <div className="title">
                                Un engagement différent et plus global
                                </div>
                                <div className="desc">
                                Affirmer publiquement votre engagement c'est le coeur du <br /> projet : que les acteurs influents affichent leur engagement <br /> écologique et aident leur public à changer.
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default MainAbout;
