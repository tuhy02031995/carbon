import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../HomePage/style.css";

const HomeIndex = () => {
    function SampleNextArrow(props:any) {
        const { className, onClick } = props;
        return (
         <div
            className={className}
            onClick={onClick}
          >
            <img src="./images/r.png" alt="" />
        </div>
        );
      }
      
      function SamplePrevArrow(props:any) {
        const { className, onClick } = props;
        return (
          <div
            className={className}
            onClick={onClick}
          >
            <img src="./images/l.png" alt="" />
        </div>
        );
      }
  const settings = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
  };
  return (
    <>
      <div className="main-section">
        <div className="banner-bf">
          <img src="./images/banner-bf.svg" alt="" />
        </div>
        <div className="container">
          <div className="main-carbon-wrap">
            <div className="main-title">
              <div className="icon">
                <img src="./images/line-title.svg" alt="" />
              </div>
              Etre carbon angel, c’est ….
            </div>
            <div className="table-carbon-wrap">
              <div className="columns">
                <div className="colum w-6">
                  <div className="box-img">
                    <img src="./images/carbon-bg.svg" alt="" />
                  </div>
                </div>
                <div className="colum w-6">
                  <div className="content-swap">
                    <ul className="list-carbon">
                      <li>Être un acteur engagé dans la société…</li>
                      <li>
                        … qui fédère sa communauté autour d’un objectif commun:
                        la réduction des gaz à effet de serre et autres
                        polluants,
                      </li>
                      <li>
                        … qui décide de renforcer sa communication auprès de son
                        public sur les actions qu’il a engagé pour réduire
                        l’impact de son activité,
                      </li>
                      <li>
                        … et qui met en place des incitations attractives et
                        nouvelles pour accompagner son public ou ses clients
                        vers des pratiques plus vertueuses.
                      </li>
                      <li>
                        mais aussi qui teste de nouvelles solutions et les
                        diffuse au plus grand nombre.
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="main-carbon-slider">
            <div className="carbon-r">
              <img src="./images/carbon-r.png" alt="" />
            </div>
            <div className="main-title">
              <div className="icon">
                <img src="./images/line-title.svg" alt="" />
              </div>
              Nos <span className="main-color">1er Carbon</span> angels <br />{" "}
              témoignent pour vous !
            </div>
            <div className="content-slider">
              <Slider {...settings}>
                <div className="item">
                  <div className="box-img">
                    <img src="./images/Column.svg" alt="" />
                  </div>
                </div>
                <div className="item">
                  <div className="box-img">
                    <img src="./images/Column.svg" alt="" />
                  </div>
                </div>
              </Slider>
            </div>
          </div>
          <div className="main-carbon-item">
            <div className="main-title">
              <div className="icon">
                <img src="./images/line-title.svg" alt="" />
              </div>
              Elus, acteurs économiques ou culturels : <br />
              <span className="main-color">DEVENEZ CARBON ANGEL !</span>
            </div>
            <div className="table-carbon-item">
                <div className="line-bottom res">
                    <img src="./images/line-bottom.svg" alt="" />
                  </div>
              <div className="columns">
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">
                      En savoir plus surle <br /> concept de Carbon-Angel ?
                    </div>
                    <div className="desc">
                      Parce que vous avez, chefs d’entreprises, artistes, élus
                      et responsables d’association, un pouvoir simple et
                      immense auprès de vos fans, vos clients ou vos usagers :
                      dites leur que vous souhaitez réduire votre impact carbone
                      avec eux et aidez-les à changer grâce à des incitations
                      attractives.
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row" type="button">
                        Comment cela fonctionne ?
                      </button>
                      <button className="btn btn-row" type="button">
                        Quels avantages pour vous ?
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">
                      Faire la différence et devenir Carbon angel ?
                    </div>
                    <div className="desc">
                      Ou simplement avoir une démo ou poser une question ?
                      N’hésitez pas aussi à nous laisser vos coordonnées pour
                      qu’on vous tienne au courant…
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row-s" type="button">
                        Devenez Carbon angel !
                      </button>
                      <button className="btn btn-row-s" type="button">
                        Demandez une démo ou laissez vos coordonnées
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">Vous souhaitez nous aider ?</div>
                    <div className="desc">
                      Un petit like sur les réseaux sociaux ou une rediffusion
                      et c’est déjà beaucoup !
                      <br />
                      <br />
                      Vous pouvez aussi nous aider en répondant à quelques
                      questions. Cela nous aidera à adapter le produit au plus
                      prêt de vos besoins.
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row" type="button">
                        Comment cela fonctionne ?
                      </button>
                      <button className="btn btn-row" type="button">
                        Quels avantages pour vous ?
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default HomeIndex;
