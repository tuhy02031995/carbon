import React from "react";
import "../HomePage/style.css";

const HomeNous = () => {
    return (
        <>
            <div className="main-section cus">
                <div className="banner-bf">
                    <img src="./images/banner-bf.svg" alt="" />
                </div>
                <div className="container">
                    <div className="main-nous-wrap">
                        <div className="main-title">
                            <div className="icon">
                                <img src="./images/line-title.svg" alt="" />
                            </div>
                            Une envie commune :<br /> <span className="main-color">accélérer et rendre visible</span>
                        </div>
                        <div className="desc-nous">
                            Tout part de 3 constats partagés entre <br />
                            les membres fondateurs : 
                        </div>
                        <ul className="list-nous">
                            <li>
                                <div className="icon">
                                    <img src="./images/w-1.svg" alt="" />
                                </div>
                                <div className="text">
                                    La transition écologique est possible si chacun accepte d’évoluer progressivement dans ses pratiques. C’est indispensable pour atteindre l’objectif climat de « Zero émissions nettes » en 2050.
                                </div>
                            </li>
                            <li>
                                <div className="icon">
                                    <img src="./images/w-2.svg" alt="" />
                                </div>
                                <div className="text">
                                    Cette vague de changement ne se décrète pas. Elle se construit socialement,  par l’implication de chaque acteur en position d’aider les autres à changer.
                                </div>
                            </li>
                            <li>
                                <div className="icon">
                                    <img src="./images/w-3.svg" alt="" />
                                </div>
                                <div className="text">
                                    Enfin, des changements de pratiques, répétés dans le temps et à grande échelle, ça finit par avoir un réel impact global. Cela peut même finir par devenir une nouvelle norme sociale ! 
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="main-nous-solution">
                        <div className="banner-bf-res cus">
                            <img src="./images/line-right.svg" alt="" />
                        </div>
                        <div className="table-solution">
                            <div className="columns">
                                <div className="colum w-6">
                                    <div className="content-solution">
                                        <div className="main-title">
                                            Au coeur de la solution <br /> Carbon-Angel  
                                        </div>
                                        <div className="desc">
                                            Les relations humaines et l’innovation numérique au service du changement de comportement, à travers 2 outils: 
                                        </div>
                                        <ul className="list-solution">
                                            <li>
                                                <div className="txt-top">
                                                    Des acteurs qui s’engagent et le montrent, 
                                                </div>
                                                <div className="txt-bot">
                                                    afin de  déclencher une mobilisation large de la société vers la transition écologique. Et une plateforme numérique qui met en relation une offre et une demande d’écogestes, et qui accompagne et conseille les carbon angels dans leur transition écologique. 
                                                </div>
                                            </li>
                                            <li>
                                                <div className="txt-top">
                                                    Une application de certification
                                                </div>
                                                <div className="txt-bot">
                                                    inciter et récompenser des écogestes nécessite de vérifier la réalisation du geste souhaité. C’est ce que permettra une application en cours de développement. Simplifier le suivi et quantifier les économies de CO2 et d’énergie réalisées, c’est un pas vers plus de sobriété. Et une valorisation publique renforcée de ces actions. 
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="colum w-6">
                                    <div className="guide-desc">
                                        <div className="icon-angel">
                                            <img src="./images/e-0.png" alt="" />
                                        </div>
                                        <ul className="list-images">
                                            <li>
                                                <div className="icon">
                                                    <img src="./images/e-1.png" alt="" />
                                                </div>
                                                <div className="text">
                                                    Quand l’individu et ses <br /> convictions…
                                                </div>
                                            </li>
                                            <li>
                                                <div className="icon">
                                                    <img src="./images/e-2.png" alt="" />
                                                </div>
                                                <div className="text">
                                                … engage son public, ses <br /> clients, ses salariés 
                                                </div>
                                            </li>
                                            <li>
                                                <div className="icon">
                                                    <img src="./images/e-3.png" alt="" />
                                                </div>
                                                <div className="text">
                                                …vers un peu de rêve et <br /> d’ambition.
                                                </div>
                                            </li>
                                            <li>
                                                <div className="icon">
                                                    <img src="./images/e-5.png" alt="" />
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main-wrap-people">
                        <div className="main-title">
                            <div className="icon">
                                <img src="./images/line-title.svg" alt="" />
                            </div>
                            L’équipe derrière les ailes… 
                        </div>
                        <div className="table-people">
                            <div className="columns">
                                <div className="colum w-6">
                                    <div className="content-popple">
                                        <div className="box-img">
                                            <img src="./images/s-2.svg" alt="" />
                                        </div>
                                        <div className="txt-round">
                                            <div className="txt">
                                            Vivian est architecte de formation. Depuis 2008 il participe avec succès au développement de sociétés d’ingénierie et de construction/promotion. Son amour des grands espaces et son envie farouche d’avoir un impact positif sur l’environnement le poussent à changer de voie et à s’engager en faveur du climat. Il rejoint et co-fonde le projet en y apportant ses compétences créatives et commerciales. 
                                            </div>
                                            <div className="button-people">
                                                <button className="btn btn-row-s" type="button">
                                                    Accéder à profil LinkedIn <img src="./images/like.svg" alt="" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="colum w-6">
                                    <div className="content-popple">
                                        <div className="box-img">
                                            <img src="./images/s-1.svg" alt="" />
                                        </div>
                                        <div className="txt-round">
                                            <div className="txt">
                                                Ingénieur généraliste dans la fonction publique, Rémy a orienté sa carrière sur les enjeux écologiques.  Après différents postes en services déconcentrés et au Ministère de la transition écologique, notamment au sein de l’équipe COP21 française, il décide de se lancer un nouveau défi : participer à la mobilisation volontaire des acteurs de la société.  Son rôle au sein de l’équipe : la stratégie et les solutions bas-carbone. 
                                            </div>
                                            <div className="button-people">
                                                <button className="btn btn-row-s" type="button">
                                                    Accéder à profil LinkedIn <img src="./images/like.svg" alt="" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="colum w-6">
                                    <div className="content-popple">
                                        <div className="box-img">
                                            <img src="./images/s-3.svg" alt="" />
                                        </div>
                                        <div className="txt-round">
                                            <div className="txt">
                                            Xuan est ingénieur de formation, spécialiste de la mécanique et de l’énergie avec 10 ans d'expériences de management et coaching.  Il travaille sur les projets de l’innovation digitale. Passionné par l’impact potentiel de Carbon-Angel pour lutter contre le changement climatique, il accompagne, appuie et conseille ce projet sur son temps libre. 
                                            </div>
                                            <div className="button-people">
                                                <button className="btn btn-row-s" type="button">
                                                    Accéder à profil LinkedIn <img src="./images/like.svg" alt="" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="main-wrap-contacter">
                        <div className="img-elip-round">
                            <img src="./images/elip-ab.svg" alt="" />
                        </div>
                        <div className="main-title">
                            <div className="icon">
                                <img src="./images/line-title.svg" alt="" />
                            </div>
                            Nous contacter 
                        </div>
                        <div className="desc-contacter">
                        Laissez nous votre mail et votre question, message, <br /> nous reviendrons vers vous. 
                        </div>
                        <div className="columns">
                            <div className="colum w-6">
                                <div className="content-contacter">
                                    <div className="text">
                                        Vous êtes un professionnel, une collectivité, une association
                                    </div>
                                    <div className="button-contacter">
                                        <button className="btn btn-row" type="button">
                                            Quels avantages pour vous  ?
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="colum w-6">
                                <div className="content-contacter">
                                    <div className="text">
                                    Vous êtes un particulier

                                    </div>
                                    <div className="button-contacter">
                                        <button className="btn btn-row" type="button">
                                        Quels avantages pour vous  ?

                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default HomeNous