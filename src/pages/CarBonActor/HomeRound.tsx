import React from "react";
import "../HomePage/style.css";

const HomeRound = () => {
  return (
    <>
      <div className="main-section cus">
        <div className="banner-bf">
          <img src="./images/banner-bf.svg" alt="" />
        </div>

        <div className="container">
          <div className="main-carbon-actor">
            <div className="columns">
              <div className="colum w-6">
                <div className="content-actor">
                  <div className="main-title">
                    <div className="icon">
                      <img src="./images/line-title.svg" alt="" />
                    </div>
                    Carbon actor, agir pour soi et pour la planète
                  </div>
                  <div className="desc">
                    Changer demande un effort : faites vous aider par la 1er
                    plateforme de rétribution des gestes écologiques et montrez
                    que vous aussi, vous participez à préserver la planète !
                  </div>
                  <div className="desc">
                    Nous sommes nombreux à être convaincus qu’il y a urgence à
                    agir pour enrayer les bouleversements écologiques et
                    climatiques. Mais changer n’est pas si facile. Le cœur du
                    projet de Carbon-Angel: reconnaître que changer demande un
                    effort. Pour cela, nous invitons les leaders d’opinion, les
                    leaders économiques ou artistiques, à devenir des Carbon
                    angels, c'est-à-dire des acteurs qui donnent l’exemple et
                    vous récompensent si vous adoptez ces nouvelles pratiques –
                    par des gratifications ou des tarifs préférentiels.
                  </div>
                </div>
              </div>
              <div className="colum w-6">
                <div className="box-img">
                  <img src="./images/actor-r.svg" alt="" />
                </div>
              </div>
            </div>
          </div>
          <div className="main-carbon-cmt">
            <div className="banner-bf-res">
              <img src="./images/line-right.svg" alt="" />
            </div>
            <div className="main-title">
              <div className="icon">
                <img src="./images/line-title.svg" alt="" />
              </div>
              Comment participer et recevoir <br /> des récompenses ?
            </div>
            <div className="desc-res">
              Inscrivez vous en ligne, adoptez un <br /> écogeste, et on
              s’occupe du reste
            </div>
            <div className="box-img">
              <img src="./images/img-desc.svg" alt="" />
            </div>
          </div>
          <div className="main-carbon-item res">
            <div className="main-title">
              <div className="icon">
                <img src="./images/line-title.svg" alt="" />
              </div>
              Vous souhaitez afficher votre <br /> engagement écologique ?
            </div>
            <div className="table-carbon-item">
              <div className="line-bottom res">
                <img src="./images/line-bottom.svg" alt="" />
              </div>
              <div className="columns">
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">Devenir Carbon actor</div>
                    <div className="desc">
                      N’hésitez pas aussi à nous laisser vos coordonnées pour
                      qu’on vous tienne au courant…
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row" type="button">
                        DEVENEZ Carbon-actor
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">Vous souhaitez nous aider ?</div>
                    <div className="desc">
                      Un petit like ou une rediffusion et c’est déjà beaucoup !
                      <br />
                      <br />
                      Vous pouvez aussi nous aider en répondant à quelques
                      questions. Cela nous aidera à adapter le produit au plus
                      prêt de vos besoins.
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row-s" type="button">
                        Dites nous quel Carbon actor vous pensez être !
                      </button>
                    </div>
                  </div>
                </div>
                <div className="colum w-4 res">
                  <div className="box-guide-w">
                    <div className="title">
                      Vous êtes déjà engagés sur un projet ?
                    </div>
                    <div className="desc">
                      Ce petit questionnaire est pour vous !
                    </div>
                    <div className="box-button">
                      <button className="btn btn-row" type="button">
                        Partagez votre expérience sur un projet en cours
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="main-carbon-pour">
              <div className="main-title">
                Pourquoi vouloir être <br /> Carbon actor ?
              </div>
              <div className="desc-pour">Car vous êtes….</div>
              <div className="table-main-pour">
                <div className="box-img">
                  <img src="./images/img-pour.svg" alt="" />
                </div>
                <ul className="list-pour">
                  <li>
                    <div className="icon">
                      <img src="./images/Vector.svg" alt="" />
                    </div>
                    <div className="text">
                      <span className="txt-top">
                        … déjà engagées et vous avez envie qu’on reconnaisse vos
                        efforts
                      </span>
                      <span className="txt-bot">
                        Garder motivées les personnes engagées. A quoi
                        pense-t-on quand on attache son vélo à côté d’un
                        imposant SUV ? Être motivé, c’est bien. Le rester dans
                        la durée, en étant reconnu dans ses efforts, c’est
                        encore mieux !
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className="icon">
                      <img src="./images/Vector.svg" alt="" />
                    </div>
                    <div className="text">
                      <span className="txt-top">
                        … déjà engagées et vous avez envie qu’on reconnaisse vos
                        efforts
                      </span>
                      <span className="txt-bot">
                        Garder motivées les personnes engagées. A quoi
                        pense-t-on quand on attache son vélo à côté d’un
                        imposant SUV ? Être motivé, c’est bien. Le rester dans
                        la durée, en étant reconnu dans ses efforts, c’est
                        encore mieux !
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className="icon">
                      <img src="./images/Vector.svg" alt="" />
                    </div>
                    <div className="text">
                      <span className="txt-top">
                        … déjà engagées et vous avez envie qu’on reconnaisse vos
                        efforts
                      </span>
                      <span className="txt-bot">
                        Garder motivées les personnes engagées. A quoi
                        pense-t-on quand on attache son vélo à côté d’un
                        imposant SUV ? Être motivé, c’est bien. Le rester dans
                        la durée, en étant reconnu dans ses efforts, c’est
                        encore mieux !
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className="icon">
                      <img src="./images/Vector.svg" alt="" />
                    </div>
                    <div className="text">
                      <span className="txt-top">
                        … déjà engagées et vous avez envie qu’on reconnaisse vos
                        efforts
                      </span>
                      <span className="txt-bot">
                        Garder motivées les personnes engagées. A quoi
                        pense-t-on quand on attache son vélo à côté d’un
                        imposant SUV ? Être motivé, c’est bien. Le rester dans
                        la durée, en étant reconnu dans ses efforts, c’est
                        encore mieux !
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="round-btn">
              <button className="btn btn-row-s" type="button">
                        Demandez une démo ou laissez vos coordonnées
                      </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default HomeRound;
