import React from "react";
import { Layout } from "antd";

import Header from './Header'
import Footer from './Footer'

const {  Content } = Layout;
function Main ({ children }: any) {

    return (
        <Layout>
            <Header />
            <Content>{children}</Content>
            <Footer />
        </Layout>
    )
}

export default Main;
