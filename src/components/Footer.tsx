import React from 'react';
import './style.css'

const Footer = () => {
    return (
        <>
            <footer className="main-footer">
                <div className="container">
                    <div className="content-footer">
                        <div className="footer-l">
                            <div className="box-logo">
                                <img src="./images/logo-carbon-svg.svg" alt="" />
                            </div>
                        </div>
                        <div className="footer-r">
                            <div className="main-menu">
                                <ul className="list-menu">
                                    <li>
                                        Carbon Angel
                                    </li>
                                    <li>
                                        Carbon Actors
                                    </li>
                                    <li>
                                        Ressources
                                    </li>
                                    <li>
                                        Partenaires
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="footer-n">
                            Nous suivre   <img src="./images/link.svg" alt="" />
                        </div>
                    </div>
                    <div className="content-bottom">
                        <ul className="list-copy">
                            <li>
                            2022 Carbon Angel. All right reserved.
                            </li>
                            <li>
                            CGU
                            </li>
                            <li>
                            Infos légales
                            </li>
                            <li>
                            Cookies Settings
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </>
    )
}
export default Footer