import React from 'react';
import './style.css'

const Header = () => {
    return (
        <>
            <header className="main-header">
                <div className="container">
                    <div className="content-header">
                        <div className="header-l">
                            <div className="box-logo">
                                <a href="/">
                                    <img src="./images/logo-carbon-svg.svg" alt="" />
                                </a>
                            </div>
                        </div>
                        <div className="header-r">
                            <div className="main-menu">
                                <ul className="list-menu">
                                    <a href="/1" >
                                        <li>
                                            Carbon Angel
                                        </li>
                                    </a>
                                    <a href="/2" >
                                        <li>
                                            Carbon Actors
                                        </li>
                                    </a>
                                    <a href="/3" >
                                        <li>
                                            Ressources
                                        </li>
                                    </a>
                                    <a href="/3" >
                                        <li>
                                            Partenaires
                                        </li>
                                    </a>
                                    <li>
                                        <button type="button" className="btn btn-app">
                                            Devenir un Carbon Angel
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}
export default Header