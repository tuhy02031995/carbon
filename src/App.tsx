// eslint-disable-next-line
import React from "react";
import {
    Route,
    Routes,
    BrowserRouter,
} from "react-router-dom";
import Main from "./components/Main";
import Home from "./pages/HomePage/Home";
import HomeIndex from "./pages/CarBonAngel/HomeIndex";
import HomeRound from "./pages/CarBonActor/HomeRound";
import HomeNous from "./pages/CarbonNous/HomeNous"
import "./App.css";

function App() {
    return (
        <div className="App">
            <Main>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/1" element={<HomeIndex />} />
                        <Route path="/2" element={<HomeRound />} />
                        <Route path="/3" element={<HomeNous />} />
                    </Routes>
                </BrowserRouter>
            </Main>
        </div>
    );
}

export default App;
